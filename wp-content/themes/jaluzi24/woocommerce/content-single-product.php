<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
//do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="content-wrap" class="container">
    <div class="page-content-r1 pb-5 border-bottom">
        <div class="conteiner-area">
            <div class="site-content">
				<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?> >
					<div class="row">
						
						<?php
						/**
						 * Hook: woocommerce_before_single_product_summary.
						 *
						 * @hooked woocommerce_show_product_sale_flash - 10
						 * @hooked woocommerce_show_product_images - 20
						 */
						do_action( 'woocommerce_before_single_product_summary' );
						?>
						<div class="summary entry-summary col-4">
							<h2 class="single-post-title"><?the_title();?></h2>
							<div class="price">
								<?=$product->get_price_html(); ?>
							</div>
							<?/*
							<div class="btn-quantity">
								<a href="javascript:void(0)" class="minus">-</a>
								<input step="1" min="1" max="" name="quantity" value="1" size="1"
								inputmode="numeric">
								<a href="javascript:void(0)" class="plus">+</a>
							</div>
							<a name="add-to-cart" class="single_add_to_cart_button mx-4" href="<?$product->add_to_cart_url()?>">Доавить в корзину</a>
							*/?>
							<form class="cart pt-4" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
								<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>
							<div class="btn-single-page d-flex">
								<div class="btn-wishlist">
									<a href="#">
										<i class="bi bi-heart"></i>
										Wishlist
									</a>
								</div>
								<div class="btn-compare mx-4">
									<a href="#">
										<i class="bi bi-shuffle"></i>
										Compare
									</a>
								</div>
							</div>
							<div class="product_meta border-top border-bottom py-4">
								<span class="sku_wrapper">Артикул:
									<a class="sku">WVN-02</a>
								</span>
								<span class="posted_in">Категория:
									<?php echo wc_get_product_category_list( $product->get_id()); ?>
								</span>
							</div>
							<div class="product-share py-4">
								<ul>
									<li class="twitter">
										<a href="#">
											<i class="bi bi-twitter"></i>
										</a>
									</li>
									<li class="facebook">
										<a href="#">
											<i class="bi bi-facebook"></i>
										</a>
									</li>
									<li class="pinterest">
										<a href="#">
											<i class="bi bi-pinterest"></i>
										</a>
									</li>
									<li class="email">
										<a href="#">
											<i class="bi bi-envelope-fill"></i>
										</a>
									</li>
								</ul>
							</div>	
						</div>
                        <div class="sitebar-container col-3">
                        <div class="textwidget">
                            <div class="box">
                                <i class="bi bi-truck"></i>
                                <span>Free shipping on orders over 1000$</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-stopwatch"></i>
                                <span>Delivery immediately after ordering</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-arrow-left-right"></i>
                                <span>Exchange and return within 3 days, simple procedure</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-file-earmark-text"></i>
                                <span>Supplier invoice for this product</span>
                            </div>
                        </div>
                    </div>
					</div>
					<div class="page-content-r2">
        <div class="tabs-wrapper pt-5">
            <ul class="tabs">
                <li class="description_tab active">
                    <a href="#">Описание</a>
                </li>
                <li class="reviews_tab">
                    <a href="#">Отзывы</a>
                </li>
                <?if (get_field('proizvoditel')):?>
                <li class="about-brands">
                    <a href="#">Производитель</a>
                </li>
                <?endif;?>
            </ul>
            <div class="description-content border p-4" id="tab-description">
                <?the_content();?>
            </div>
            <div class="reviews-content border p-4" id="tab-reviews" style="display:none;">
                <?php comments_template();?>
            </div>
            <?if (get_field('proizvoditel')):?>
            <div class="about-brands-content border p-4" id="tab-about-brands" style="display:none;">
                <?the_field('proizvoditel')?>
            </div>
            <?endif;?>
        </div>
            <?do_shortcode('[related_products per_page="5" columns="5"]');?>
        
					<?php
					/**
					 * Hook: woocommerce_after_single_product_summary.
					 *
					 * @hooked woocommerce_output_product_data_tabs - 10
					 * @hooked woocommerce_upsell_display - 15
					 * @hooked woocommerce_output_related_products - 20
					 */
					do_action( 'woocommerce_after_single_product_summary' );
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
