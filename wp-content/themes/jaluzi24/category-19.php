<?php


get_header();
?>
<div id="content-wrap" class="container-xxl">
    <div class="row">
    <?php get_template_part('sidebar_left'); ?>
        <section class="col-12 col-lg-9 order-0 order-lg-1 blog__entries">
        <?php
            $query = new WP_Query( 'cat=19&posts_per_page=5' ); 
            while (have_posts()) : the_post();
                setup_postdata( $post );
                ?>
                    <article class="row col-12 col-md-12 align-items-center blog__entry">
                        <div class="col-12 col-md-6 entry__image">
                            <a class="entry__link" href="<?= the_permalink(); ?>">
                                <?php the_post_thumbnail(); ?>
                            </a>
                        </div>                
                        <div class="col-12 col-md-6 entry__description">
                            <h2 class="entry__heading">
                                <a class="entry__link" href="<?= the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p class="entry__preview"><?php the_truncated_post( 400 );?></p>
                            <div class="entry__bottom">
                                <div class="entry__comments">
                                    <i class="bi bi-chat"></i>
                                    <a href="#"><?comments_number();?></a>
                                </div>
                                <span class="entry__date"><?php get_the_date(); ?></span>
                            </div>
                        </div>                
                    </article>
                <?php 
            endwhile;
            wp_reset_postdata();
            ?>
            <?the_posts_pagination();?>
            <?php echo category_description(); ?>
        </section>
    </div>
</div>
<?php get_footer();?>
