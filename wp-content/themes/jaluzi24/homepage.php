<?
/*
 * Template name: Главная
 * Template post type: page
 */
get_header();
?>
<?php
echo do_shortcode('[smartslider3 slider="2"]');
?>
  <!--Карточки товаров-->
  <div class="container-fluid py-4">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
      <div class="col">
        <div class="card">
          <a href="#">
            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner-01.jpg" class="card-img" alt="...">
          </a>        
        </div>
      </div>
      <div class="col">
        <div class="card">
          <a href="#">
            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner-02.jpg" class="card-img" alt="...">
          </a>          
        </div>
      </div>
      <div class="col d-none d-md-block">
        <div class="card">
          <a href="#">
            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner-03.jpg" class="card-img" alt="...">
          </a>          
        </div>
      </div>    
    </div>
  </div>  
  <!-- Заголовок 1 -->
  <div class="col-12 d-flex flex-column align-items-center justify-content-center py-4">
    <h3>Популярные категории</h3>
    <p class="text-muted">Add Popular Categories To Weekly Line Up</p>
  </div>
  <!-- Слайдер популярных категорий -->
  <div class="container">
      <?php $product_categories = get_terms(array(

          'taxonomy' => 'product_cat',
          'orderby'    => 'count',
          'order'      => 'DESC',
          'hide_empty' => false

      )); ?>
      <?php if($product_categories): ?>
      <div class="swiper">
          <div class="swiper-wrapper">
          <?php foreach($product_categories as $temp): ?>
              <?php $link = get_term_link($temp->term_id); ?>
              <?php $category_thumbnail = get_term_meta($temp->term_id, 'thumbnail_id', true);
              $image = wp_get_attachment_url($category_thumbnail);
              ?>
              <div class="swiper-slide">
                  <div class="card border-0">
                      <img class="card-img-top border" style="max-height: 170px" src="<?php echo $image; ?>" alt="<?php echo $temp->name; ?>" />
                      <div class="card-body">
                          <a href="<?php echo $link; ?>"><h5 class="card-title"><?php echo $temp->name; ?></h5></a>
                          <p class="card-text text-muted">Товаров в категории</p>
                          <a href="<?php echo $link?>" class="btn btn-primary">
                              В каталог
                              <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-play-circle-fill" viewBox="0 0 16 16">
                                  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/>
                              </svg>
                          </a>
                      </div>
                  </div>
              </div>
          <?php endforeach; ?>
          </div>
          <!-- <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div> -->
      </div>
      <?php endif; ?>
  </div>
  <div class="container">
    <div class="col-12 d-flex flex-column align-items-center justify-content-center py-4">
      <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner-04.jpg">
    </div> 
  </div>
  <!-- Заголовак 2 -->
  <div class="col-12 d-flex flex-column align-items-center justify-content-center py-4">
    <h3>New Arrivals</h3>
    <p class="text-muted">Add new products to weekly line up</p>
  </div> 
  <!-- Прайс -->
  <div class="container">
    <div class="row row-cols-2 row-cols-sm-3 row-cols-lg-4 row-cols-xl-6 g-3">
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-1-1-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>           
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">
              </div>
            </div>           
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-9-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
                <span class="price">$290.00</span>
                <div class="stra-rating">                 
                </div>
              </div>           
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-3-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-6-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-5-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-19-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div> 
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-18-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-12-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-15-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-16-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-11-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card border_hover">
          <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-17-300x300.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <div class="category">
              <a href="#">CATEGORY NAME 04</a>
            </div>
            <div class="title">
              <a href="#">Название карточки</a>
            </div> 
            <div class="inner">
              <span class="price">$290.00</span>
              <div class="stra-rating">                
              </div>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Заголовок 3 -->
  <div class="container">
    <div class="row row-cols-1 row-cols-sm-2 py-4 g-3">
      <div class="col">
        <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner5.jpg">
      </div>
      <div class="col">
        <img src="/wp-content/themes/jaluzi24/app/imgoptimize/banner6.jpg">
      </div>
    </div>
  </div>  
  <!-- Заголовк 4  -->
  <div class="col-12 d-flex flex-column align-items-center justify-content-center py-4">
    <h3>Hot Deals </h3>
    <p class="text-muted">Add hot products to weekly line up </p>
  </div>
  <!-- Горячие предложения -->
  <div class="container">
    <div class="swiper swiper-deals">      
      <div class="swiper-wrapper">        
        <div class="swiper-slide">
          <div class="card mb-3 border_hover" style="max-width: auto;">
          <div class="row g-0">
            <div class="col-5">
              <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-1-1-300x300.jpg" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-7">
              <div class="card-body">
                <div class="category">
                  <a href="#">CATEGORY NAME 4</a>
                </div>           
                <div class="title">
                  <a href="#">Название карточки</a>
                </div> 
                <div class="inner">
                  <span class="price">$290.00</span>
                  <div class="stra-rating">
                  </div>
                </div>           
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
          <div class="card mb-3 border_hover" style="max-width: auto;">
          <div class="row g-0">
            <div class="col-5">
              <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-12-300x300.jpg" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-7">
              <div class="card-body">
                <div class="category">
                  <a href="#">CATEGORY NAME 4</a>
                </div>           
                <div class="title">
                  <a href="#">Название карточки</a>
                </div> 
                <div class="inner">
                  <span class="price">$290.00</span>
                  <div class="stra-rating">
                  </div>
                </div>           
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
          <div class="card mb-3 border_hover" style="max-width: auto;">
          <div class="row g-0">
            <div class="col-5">
              <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-15-300x300.jpg" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-7">
              <div class="card-body">
                <div class="category">
                  <a href="#">CATEGORY NAME 4</a>
                </div>           
                <div class="title">
                  <a href="#">Название карточки</a>
                </div> 
                <div class="inner">
                  <span class="price">$290.00</span>
                  <div class="stra-rating">
                  </div>
                </div>           
              </div>
            </div>
          </div>
        </div>        
      </div>     
      </div>     
    </div>  
  </div>

<section class="latest-blog">
    <div class="col-12 d-flex flex-column align-items-center justify-content-center py-4">
        <h3>Последние записи в блоге</h3>
    </div>
    <div class="container">
        <div class="swiper swiper-deals">
            <div class="swiper-wrapper">
                <?php
                global $post;
                $myposts = get_posts([
                    'numberposts' => 10,
                    'category'    => 19
                ]);

                if( $myposts ){
                    foreach( $myposts as $post ){
                        setup_postdata( $post );
                        ?>
                        <article class="col-12 col-md-6 col-lg-4 align-items-center blog__entry swiper-slide">
                            <div class="entry__image">
                                <a class="entry__link" href="<?= the_permalink(); ?>">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            </div>
                            <div class="entry__description">
                                <a href="#"></a>
                                <h2 class="entry__heading">
                                    <a class="entry__link" href="<?= the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <p class="entry__preview"><?php the_truncated_post( 95 );?></p>
                                <div class="entry__bottom">
                                    <div class="entry__comments">
                                        <i class="bi bi-chat"></i>
                                        <a href="#">0 Comments</a>
                                    </div>
                                    <span class="entry__date"><?php get_the_date(); ?></span>
                                </div>
                            </div>
                        </article>
                        <?php
                    }
                } else {
                }
                wp_reset_postdata();

                ?>
            </div>
        </div>
    </div>
</section>

  <?php

get_footer();