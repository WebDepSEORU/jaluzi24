//Файл для написания сценария работы Gulp
let projectFolder = "app"; // Переменная, которая содержит имя папки, в которую будет выводиться результат работы Gulp
let sourceFolder = "src"; // Переменная, которая содержит имя папки с исходниками 

let path = {
    //Объекты
    build: {
        //Пути к файлам и папкам, куда будет выгружаться проект
        css: projectFolder + "/css/",
        img: projectFolder + "/imgoptimize/",
        js: projectFolder + "/js/",

    },
    src: {
        //Пути к файлам и папкам с исходниками
        css: sourceFolder + "/scss/style.scss",
        img: sourceFolder + "/img/**/*.{jpg,png,svg,webp,gif,ico}",
        js: sourceFolder + "/js/script.js",
    },
    watch: {
        css: sourceFolder + "/scss/**/*.scss",
        img: sourceFolder + "/img/**/*.{jpg,png,svg,webp,gif,ico}",
        js: sourceFolder + "/js/**/*.js",
    },
    clean: projectFolder
}

let { src, dest } = require('gulp'),
    gulp = require('gulp'), // Объявление Gulp
    browserSync = require("browser-sync").create(); //Объявление плагина Browser-Sync
    fileInclude = require("gulp-file-include"); //Объявление плагина gulp-file-include
    delDistr = require("del"); //Объявление плагина gulp-file-include
    scss = require("gulp-sass")(require('sass')); //Объявление плагина gulp-sass (чтобы корректно работал плагин пришлось поставить sass 'npm i sass --save-dev')
    autoprefixer = require("gulp-autoprefixer"); 
    groupMedia = require("gulp-group-css-media-queries"); 
    clean_css = require("gulp-clean-css"); 
    rename = require("gulp-rename");
    uglify = require("gulp-uglify-es").default; 
    imagemin = require("gulp-imagemin");

// Функция инициализации плагина browser-sync для запуска проекта из папки Dist
function browsersync(params){
    browserSync.init({
        proxy: 'http://jaluzi24.local',
        host: 'jaluzi24.local',
        open: 'external'        
    });
}

function imagesEditor() {
    return src(path.src.img)
        .pipe(imagemin(
            {
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                interlaced: true,
                optimizationLevel: 3 // от 0 до 7
            }
        ))
        .pipe(dest(path.build.img))
        .pipe(browserSync.stream())
}

function cssWork(){
    return src(path.src.css)
        .pipe(fileInclude())
        .pipe(
            scss({
                outputStyle: "expanded" //Формирование файла без минимизации\сжатия
            })
        )
        .pipe(
            groupMedia()
        )
        .pipe(dest(path.build.css))
        .pipe(
            clean_css()
        )
        .pipe(
            rename({
                extname: ".min.css"
            })
        )
        .pipe(
            autoprefixer({
                overrodeBrowserlist: ["last 5 versions"], // Браузеры, которые нужно поддерживать(последния 5 версий)
                cascade: true // Стиль написания автопрефиксера
            })
        )
        .pipe(dest(path.build.css))
        .pipe(browserSync.stream())
}

function js() {
    return src(path.src.js)
        .pipe(fileInclude())    
        .pipe(dest(path.build.js))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: ".min.js"
            })
        )
        .pipe(dest(path.build.js))
        .pipe(browserSync.stream())
}

// Фукнция отслеживания изменения html, scss файлов
function watchFiles(params){
    gulp.watch([path.watch.css], cssWork);
    gulp.watch([path.watch.img], imagesEditor);
    gulp.watch([path.watch.js], js);
}

//Функция очистки папки distr
function clean(params){
    return delDistr(path.clean)
}

let build = gulp.series(clean, gulp.parallel(cssWork, js, imagesEditor));
//Сценарий выполнения
let watch = gulp.parallel(build, watchFiles, browsersync);

exports.js = js;
exports.imagesEditor = imagesEditor;
exports.cssWork = cssWork;
exports.build = build;
exports.watch = watch;
exports.default = watch;