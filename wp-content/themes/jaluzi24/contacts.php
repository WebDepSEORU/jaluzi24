<?
/*
* Template name: Контакты
* Template post type: page
*/
get_header();?>
<div id="content-wrap" class="container-xxl">
    <div class="container">
        <div class="left-block">
            <?the_content();?>
        </div>
        <div class="right-block"></div>
    </div>
</div>

<?get_footer();?>