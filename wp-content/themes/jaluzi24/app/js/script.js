var swiper = new Swiper(".swiper", {
  loop: true,
  freeMode: false,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },  
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20      
    },
    767: {
      slidesPerView: 2,
      spaceBetween: 30
    },
    1024: {
      slidesPerView: 4,   
      spaceBetween: 40 
    }
  }
});

  var swiper = new Swiper(".swiper-deals", {
    watchSlidesProgress: true,
    spaceBetween: 30,
    loop: true,
    freeMode: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: { 
      320: {
        slidesPerView: 1,
        spaceBetween: 20      
      },
      767: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      1024: {
        slidesPerView: 3,   
        spaceBetween: 40 
      }
    }
  });

  var swiper = new Swiper(".swiper-latest-posts", {
    watchSlidesProgress: true,
    spaceBetween: 30,
    loop: true,
    freeMode: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: { 
      320: {
        slidesPerView: 1,
        spaceBetween: 20      
      },
      767: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    }
  });

// Счётчик количества товара

$('.plus').on('click', function(e) {
  var val = parseInt($(this).prev('input').attr('value'));
  $(this).prev('input').attr('value', val + 1);
});
 
$('.minus').on('click', function(e) {
  var val = parseInt($(this).next('input').attr('value'));
    if (val !== 1) {
        $(this).next('input').attr('value', val - 1);
    }
});

// Добавление атрибутов для шапки

jQuery(document).ready(function (){
  jQuery('.dropdown-menu').attr("aria-labelledby", "navbarDropdown");
  jQuery('a[role="button"]').addClass('dropdown-toggle');
  jQuery('.menu-item-has-children li a.nav-link').addClass('dropdown-item');
  jQuery('.menu-item-has-children li a.dropdown-item').removeClass('nav-link');

});

// Переключение табов

$(document).ready(function(){
    $('.tabs-wrapper li').click(function() {
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
    });
});

$('.description_tab').click(function() {
  $('#tab-description').css("display", "block");
  $('#tab-reviews, #tab-about-brands').css("display", "none");
});

$('.reviews_tab').click(function() {
  $('#tab-description, #tab-about-brands').css("display", "none");
  $('#tab-reviews').css("display", "block");
});

$('.about-brands').click(function() {
  $('#tab-description, #tab-reviews').css("display", "none");
  $('#tab-about-brands').css("display", "block");
});

