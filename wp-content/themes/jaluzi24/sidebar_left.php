<aside class="col-12 col-lg-3 order-1 order-lg-0 widget__sidebar">
    <input class="widget__search" placeholder="Search" type="search">
    <div class="categories__wrapper">
        <h4 class="block__title">Categories</h4>
        <ul class="row text-left category__list">
            <li class="category__item"><a class="category__link" href="#">Category Name 01</a></li>
            <li class="category__item"><a class="category__link" href="#">Category Name 02</a></li>
            <li class="category__item"><a class="category__link" href="#">Category Name 03</a></li>                    
        </ul>
    </div>            
    <div class="recents__posts">
        <h4 class="block__title">Recent Post</h4>
        <ul class="text-left post__list">              
            <?php
            global $post;

            $myposts = get_posts([ 
                'numberposts' => 4,
                'category'    => 19
            ]);

            if( $myposts ){
                foreach( $myposts as $post ){
                    setup_postdata( $post );
                    ?>
                    <li class="post__item">
                        <a class="post__link" href="<?= the_permalink(); ?>">
                        <?php the_post_thumbnail(
                            array(),
                            array(
                                'class' => 'post__image'
                            )
                        ); ?>
                        </a>
                        <div class="description__block">
                            <a class="post__link" href="<?= the_permalink(); ?>" >
                                <p class="post__description"><?php the_title(); ?></p>
                            </a>
                            <span class="post__date"><?php the_date(); ?></span>
                        </div>                       
                    </li>
                    <?php 
                }
            } else {
                // Постов не найдено
            }

            wp_reset_postdata(); // Сбрасываем $post
            ?>                              
        </ul>
    </div>           
</aside>