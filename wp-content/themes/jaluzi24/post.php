<?
/*
 * Template name: Статья
 * Template post type: post
 */
get_header();
?>
<div id="content-wrap" class="container-xxl">
    <div class="row">
        <?php get_template_part('sidebar_left'); ?>
        <section class="col-12 order-0 order-md-0 col-lg-9 order-lg-1 blog__entries">
            <article class="row col-12 col-md-12 align-items-center">
                <div class="col-12 pb-4 entry__image">
                    <?php the_post_thumbnail(); ?>
                </div>                
                <div class="col-12 px-0 entry__description">
                    <h2 class="entry__heading">
                        <a class="entry__link" href="#"><?php the_title(); ?></a>
                    </h2>
                    <ul class="meta">
                        <li class="meta-author">
                            <i class="bi bi-person"></i>                          
                            <a href="#">admin</a>
                        </li>
                        <li class="meta-date">
                            <i class="bi bi-clock"></i>
                            <a><?=the_date('Y-m-d');?></a>
                        </li>
                        <li class="meta-cat">
                            <i class="bi bi-folder"></i>  
                            <a href="#">Category name 03</a>
                        </li>   
                        <li class="meta-comments">
                            <i class="bi bi-chat"></i>
                            <a href="#">0 Comments</a>
                        </li>                  
                    </ul>                
                    <div class="entry__preview"><?php the_content(); ?></div>
                </div>
                <div class="related-posts px-0 border-bottom">
                    <h3 class="theme-heading">                        
                        <span class="text">
                        <i class="bi bi-chevron-right"></i>
                        You Might Also Like
                        </span>
                    </h3>
                    <div class="col-4 text-center mb-4">
                        <a href="#">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/blog-2.jpg"></img>
                        </a>
                        <h3 class="related-post-title">
                            <a href="#">Sample post title with format chat</a>
                        </h3>
                        <time>
                            <i class="bi bi-clock"></i>
                            <a><? get_the_date(); ?></a>
                        </time>
                    </div>
                </div>
                <?php 
                    $comments = get_comments( array( 'post_id' => get_the_ID() ) );
                    foreach ( $comments as $comment ) :?>
                        <h4><?=$comment->comment_author;?></h4>
                        <p><?=$comment->comment_content;?></p>
                    <? endforeach; 
                ?>
                <?php comment_form(); ?> 
            </article>    
        </section>
    </div>    
</div>

<?php get_footer();?>
