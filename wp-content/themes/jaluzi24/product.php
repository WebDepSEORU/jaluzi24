<?
/*
 * Template name: Товар
 * Template post type: product
 */
get_header();
?>
<div id="content-wrap" class="container">
    <div class="page-content-r1 pb-5 border-bottom">
        <div class="conteiner-area">
            <div class="site-content">
                <div class="row">
                    <div class="ecommerce-gallery col-5" data-mdb-zoom-effect="true" data-mdb-auto-height="true">
                        <div class="row shadow-5">
                            <div class="col-12 mb-3">
                                <div class="lightbox">
                                    <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-15.jpg"
                                    alt="Gallery image 1" class="ecommerce-gallery-main-img active w-100" />
                                </div>
                            </div>
                            <div class="col-3 mt-1">
                                <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-15.jpg"
                                alt="Gallery image 1" class="active w-100" />
                            </div>
                            <div class="col-3 mt-1">
                                <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-17.jpg"
                                alt="Gallery image 2" class="w-100" />
                            </div>
                            <div class="col-3 mt-1">
                                <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-18.jpg"
                                alt="Gallery image 3" class="w-100" />
                            </div>
                        </div>
                    </div>
                    <div class="col-4">                        
                        <h2 class="single-post-title">Downloadable Product 001</h2>
                        <div class="price">
                            <span>$180.00</span>
                        </div>
                        <div class="product-details__short-description py-4">
                            <div class="textproduct">
                                <ul>
                                    <li>
                                        <i class="bi bi-check2-circle"></i>
                                        Hand-finished
                                    </li>
                                    <li>
                                        <i class="bi bi-check2-circle"></i>
                                        Includes 1 coffee table and 2 end tables
                                    </li>
                                    <li>
                                        <i class="bi bi-check2-circle"></i>
                                        Made of glass and metal
                                    </li>
                                    <li>
                                        <i class="bi bi-check2-circle"></i>
                                        Assembly required
                                    </li>
                                    <li>
                                        <i class="bi bi-check2-circle"></i>
                                        Beveled glass tabletops with black underlays
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="cart py-4">
                            <div class="d-flex">
                                <div class="btn-quantity">
                                    <a href="javascript:void(0)" class="minus">-</a>
                                    <input step="1" min="1" max="" name="quantity" value="1" size="1"
                                    inputmode="numeric">
                                    <a href="javascript:void(0)" class="plus">+</a>
                                </div>
                                <button type="submit" name="add-to-cart" class="single_add_to_cart_button mx-4">Add
                                    to
                                    cart</button>
                            </div>
                            <div class="btn-single-page d-flex">
                                <div class="btn-wishlist">
                                    <a href="#">
                                        <i class="bi bi-heart"></i>
                                        Wishlist
                                    </a>
                                </div>
                                <div class="btn-compare mx-4">
                                    <a href="#">
                                        <i class="bi bi-shuffle"></i>
                                        Compare
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_meta border-top border-bottom py-4">
                            <span class="sku_wrapper">SKU:
                                <a class="sku">WVN-02</a>
                            </span>
                            <span class="posted_in">Category:
                                <a href="#" rel=tag>Category Name 01</a>
                            </span>
                            <span class="tagged_as">Tags:
                                <a href="#" rel=tag>Tag-02</a>
                                <a href="#" rel=tag>Tag-03</a>
                                <a href="#" rel=tag>Tag-04</a>
                                <a href="#" rel=tag>Tag-05</a>
                            </span>
                        </div>
                        <div class="product-share py-4">
                            <ul>
                                <li class="twitter">
                                    <a href="#">
                                        <i class="bi bi-twitter"></i>
                                    </a>
                                </li>
                                <li class="facebook">
                                    <a href="#">
                                        <i class="bi bi-facebook"></i>
                                    </a>
                                </li>
                                <li class="pinterest">
                                    <a href="#">
                                        <i class="bi bi-pinterest"></i>
                                    </a>
                                </li>
                                <li class="email">
                                    <a href="#">
                                        <i class="bi bi-envelope-fill"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sitebar-container col-3">
                        <div class="textwidget">
                            <div class="box">
                                <i class="bi bi-truck"></i>
                                <span>Free shipping on orders over 1000$</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-stopwatch"></i>
                                <span>Delivery immediately after ordering</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-arrow-left-right"></i>
                                <span>Exchange and return within 3 days, simple procedure</span>
                            </div>
                            <div class="box">
                                <i class="bi bi-file-earmark-text"></i>
                                <span>Supplier invoice for this product</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-r2">
        <div class="tabs-wrapper pt-5">
            <ul class="tabs">
                <li class="description_tab active">
                    <a href="#">Description</a>
                </li>
                <li class="reviews_tab">
                    <a href="#">Reviews</a>
                </li>
                <li class="about-brands">
                    <a href="#">About Brands</a>
                </li>
            </ul>
            <div class="description-content border p-4" id="tab-description">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique
                    auctor.</p>
                <p>Ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies
                    massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo
                    augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et
                    placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi.</p>
                <p>Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet
                    ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
                    turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus, ultricies posuere erat. Duis convallis,
                    arcu nec aliquam consequat, purus felis vehicula felis, a dapibus enim lorem nec augue. Nunc
                    facilisis sagittis ullamcorper.</p>
            </div>
            <div class="reviews-content border p-4" id="tab-reviews" style="display:none;">
                <ol class="commentlist">
                    <li class="comment">
                        <div id="comment-24" class="comment_container">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/avatar.png" class="avatar">
                            <div class="comment-text">
                                <p class="meta">
                                    <strong class="author">admin</strong>
                                    <span class="dash">-</span>
                                    <time class="published-date">Январь 10, 2022</time>
                                </p>
                                <div class="description">
                                    <p>Aliquam fringilla euismod risus ac bibendum. Sed sit amet sem varius ante feugiat
                                        lacinia. Nunc ipsum nulla, vulputate ut venenatis vitae, malesuada ut mi.
                                        Quisque iaculis, dui congue placerat pretium, augue erat accumsan lacus</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div id="review_form_wrapper">
                    <div id="review_form">
                        <div id="respond" class="comment-respond">
                            <span id="reply-title" class="comment-reply-title">
                                Add a review
                            </span>
                            <form id="commentform" class="comment-form">
                                <p class="comment-notes">
                                    <span id="email-notes">Your email address will not be published. Required fields are
                                        marked</span>
                                    <span class="required">*</span>
                                </p>
                                <div class="comment-form-rating">
                                    <label for="rating">
                                        Your rating
                                        <span class="required">*</span>
                                    </label>
                                    <p class="stars">
                                        <span>
                                            <a class="star-1" href="#">1</a>
                                            <a class="star-2" href="#">2</a>
                                            <a class="star-3" href="#">3</a>
                                            <a class="star-4" href="#">4</a>
                                            <a class="star-5" href="#">5</a>
                                        </span>
                                    </p>
                                </div>
                                <p class="comment-form-comment">
                                    <label for="rating">
                                        Your review
                                        <span class="required">*</span>
                                    </label>
                                    <textarea id="comment" name="comment" cols="45" rows="8" required=""></textarea>
                                </p>
                                <p class="comment-form-author">
                                    <label for="author">
                                        Name
                                        <span class="required">*</span>
                                    </label>
                                    <input id="author" name="author" type="text" value="" size="30" required="">
                                </p>
                                <p class="comment-form-email">
                                    <label for="email">
                                        Email
                                        <span class="required">*</span>
                                    </label>
                                    <input id="email" name="email" type="email" value="" size="30" required="">
                                </p>
                                <p class="form-submit">
                                    <input name="submit" type="submit" id="submit" class="submit" value="Submit">
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-brands-content border p-4" id="tab-about-brands" style="display:none;">
                <h4>Aenean cursus ligula ut aliquet consectetur?</h4>
                <p>Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                <h4>Morbi sed sapien velit tincidunt, libero non semper vulputate</h4>
                <p>Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                <h4>Sed tincidunt, libero non semper vulputate, mauris</h4>
                <p>Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
            </div>
        </div>
        <section class="related-products">
            <h2>Related products</h2>
            <div class="container">
                <div class="row row-cols-2 row-cols-sm-3 row-cols-lg-4 row-cols-xl-5 g-3">
                    <div class="col">
                        <div class="card border_hover">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-1-1-300x300.jpg"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="category">
                                    <a href="#">CATEGORY NAME 04</a>
                                </div>
                                <div class="title">
                                    <a href="#">Название карточки</a>
                                </div>
                                <div class="inner">
                                    <span class="price">$290.00</span>
                                    <div class="stra-rating">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card border_hover">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-9-300x300.jpg"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="category">
                                    <a href="#">CATEGORY NAME 04</a>
                                </div>
                                <div class="title">
                                    <a href="#">Название карточки</a>
                                </div>
                                <div class="inner">
                                    <span class="price">$290.00</span>
                                    <div class="stra-rating">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card border_hover">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-3-300x300.jpg"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="category">
                                    <a href="#">CATEGORY NAME 04</a>
                                </div>
                                <div class="title">
                                    <a href="#">Название карточки</a>
                                </div>
                                <div class="inner">
                                    <span class="price">$290.00</span>
                                    <div class="stra-rating">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card border_hover">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-6-300x300.jpg"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="category">
                                    <a href="#">CATEGORY NAME 04</a>
                                </div>
                                <div class="title">
                                    <a href="#">Название карточки</a>
                                </div>
                                <div class="inner">
                                    <span class="price">$290.00</span>
                                    <div class="stra-rating">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card border_hover">
                            <img src="/wp-content/themes/jaluzi24/app/imgoptimize/product-5-300x300.jpg"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="category">
                                    <a href="#">CATEGORY NAME 04</a>
                                </div>
                                <div class="title">
                                    <a href="#">Название карточки</a>
                                </div>
                                <div class="inner">
                                    <span class="price">$290.00</span>
                                    <div class="stra-rating">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>
    </div>
</div>

<?php get_footer();?>