<div class="container-fluid d-flex justify-content-center bg__wrapper">
    <div class="row">
        <div class="page__title text-center">
            <h1 class="page__heading">
                <?php single_cat_title(); ?></h1>
            <nav class="row d-flex justify-content-center" style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<span id="breadcrumbs">','</span>' );
                    }
                    ?>
                </ol>
            </nav>
        </div>
    </div>
</div>